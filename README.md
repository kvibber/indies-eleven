---
title: "About Indie's Eleven"
originalDate: "2022-06-16T20:00:00-0700"
date: "2025-01-04T20:14:46-08:00"
layout: article.njk
schemaType: TechArticle
authorId: KelsonV
canonical: https://codeberg.org/kvibber/indies-eleven
alternates:
  Codeberg: "https://codeberg.org/kvibber/indies-eleven"
---
# README

There's no reason to make someone download 500 KB of images, JavaScript, extra styles and cluttered HTML code just to read a 500-word blog post. That's fine on a flagship phone on a 5G connection, or your high-end PC on WiFi hooked up to fiber-optic cable, but not everyone has those. And even high-end mobile devices are limited by what signal they can pick up.

Indie's Eleven is a theme for Eleventy designed to be as **light as possible** (need for AMP on mobile!) on the client while still supporting:

* HTML5 structure and metadata including [Microformats2](http://microformats.org/wiki/microformats2) ([IndieWeb](https://indieweb.org/) compatible sites), Schema.org (Google), [OpenGraph](https://ogp.me/) (Facebook, LinkedIn), and [Twitter Cards](https://developer.twitter.com/en/docs/twitter-for-websites/cards/overview/markup).
* Display sizes from phones all the way up to widescreen monitors.
* Automatic light/dark mode switching.

The base layout doesn't use any images, though it does specify icons that can be used for tabs/bookmarks, and preview images for incoming social media links. You will want to change (or remove) these - they're both generic and boring. It uses CSS, [system fonts](https://systemfontstack.com/), Unicode symbols and emoji. So a simple text post *only needs two requests* to render: the HTML and the stylesheet.

So your pages should load super-fast even on low-bandwidth connections or low-power devices!

A big thank-you to [eleventy-base-blog](https://github.com/11ty/eleventy-base-blog), which helped me figure out how to get started, and I ended up using parts of that project's .eleventy.js and feed.njk.

You can use this via the [MIT License](LICENSE). If you're just using it for your own site, make whatever changes you want. If you want to share your changes, read the license for its terms.

## TODO

* Post example site and link to it from README
* Finish documentation!
* Clean up documentation
* Forward/back within category or series instead of by time.
* Clean up Stream heading levels.
* Get note titles working in the collections view
* Clean up duplicate headings (mostly for Readme)
* Test with screen readers
* Accept new comments.
* Code Cleanup
* Add [Reuse Software](https://reuse.software/) metadata to the code?

[Source on Codeberg](https://codeberg.org/kvibber/indies-eleven)  
(c) 2022-2025 Kelson Vibber [kvibber.com](https://kvibber.com) [software@kvibber.com](mailto:software@kvibber.com)
