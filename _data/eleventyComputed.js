const authors = require("./authors.json");
const siteInfo = require("./siteInfo.json");

module.exports = {
	pageInfo: {
		author: data => {
			// collect from data cascade
			
			// Is authorId in authors.json?
	    		if (authors[data.authorId]) {
	    			return authors[data.authorId];
	    		}
	    		// Is there an author structure in the front matter?
	    		if (data.author && data.author.name) {
	    			return data.author;
	    		}
	    		// Return the site's primary author
			return authors[siteInfo.author];
		}
	}
};
