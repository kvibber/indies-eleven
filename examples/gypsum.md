---
title: "Lorem! Ipsum! Gypsum!"
author: IndieJr
date: "2022-05-30T18:53:00-0700"
credits: "Author"
canonical: ""
movedFrom:
  "My Old Blog": "https://whataday.example.com"
alternates:
  "Tumblr": "https://example.tumblr.com/wherever"
tags:
  - Geology
  - Sample
description: "The time I found a bunch of gypsum."
---
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.

## Wait, what?

Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

## What does this have to do with geology?

Nothing, it's just here to show you how this works
