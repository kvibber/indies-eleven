---
title: "Watercolor Path"
date: "2022-06-10T20:00:00-07:00"
author: IndieJr
layout: photo.njk
canonical: ""
image: "images/watercolor-path.jpg"
alt: "A photo of a sunny path with green trees and blue sky in the background."
alternates:
  "Tumblr": "https://example.tumblr.com/wherever"
tags:
  - Hiking
  - Sample
description: "A sunny path"
---

The day I went out walking along a woodchip-lined path.

