---
title: "Getting Started With Indie's Eleven"
date: "2024-07-02T17:59:00-0700"
originalDate: "2022-06-16T20:01:00-0700"
layout: article.njk
schemaType: TechArticle
authorId: KelsonV
canonical: https://codeberg.org/kvibber/indies-eleven/docs/HOWTO.md
alternates:
  Codeberg: "https://codeberg.org/kvibber/indies-eleven/docs/HOWTO.md"
---

1. Install Node.js.

2. Clone or copy the Indies-Eleven files and name it what you're going to call your site.

3. From the command-line, in the root of your site's folder, run npm-install eleventy TODO double-check this

4. Edit .eleventy.js wherever you find "GETTING STARTED", currently:
* Image folder, formats, sizes.
* Category list (TODO: scrap this and pull from categories.json)
* Your time zone

## Site Info

Edit _data/siteInfo.json to set site-wide information:

``` 
"title": "Indie's Eleven",
"url": "https://example.com/indie/",
"domain": "https://example.com",
```
These should be self-explanatory.

```
"email": "webmaster@example.com",
```
This is a site-wide email contact for the home page, category pages, etc.

```
"search": "https://duckduckgo.com/?site=example.com",
```
Optional. This is the target for the search link in the top bar. It can be left out, you can point to another search engine, or you can point to your own.

Leave it out to take the search link out of the top bar.

```
"twitter": "ExampleSite",
```
Optional. The Twitter account associated with the site as a whole. Not necessarily the same as the author(s)' accounts. Used on the home page and as the site's account in Twitter cards that link to your pages. 
	
```
"author": "Indie"
```
The nickname of the author of most posts on your site. This author will be used for pages that don't list an author in the markdown file.

## Categories

**_data/categories.json** includes information about your site's categories, for instance:

```
"Docs": {
	"label": "Documentation",
	"emoji": "📚"
}
```

For now you also need to list your categories in **`.eleventy.js`**.

## Authors

You can put multiple authors in **`_data/authors.json`**, each identified by a nickname you can use in specific posts. For guest authors, you can put them in here, or you can put the same structure in the individual post's front matter.

The only required fields are the nickname and the author's name, which will appear at the end of their posts.
```
"Indie": {
	"name": "Indie Bloggs",
}
```
**The rest of these fields are optional.** If included in the author's data block, they'll be used. If not, they won't.

```
"url": "https://indie.example.com",
```
The author's home page. Will be linked in their post signature and in post metadata that supports it.

```
"twitter": "Example"
```

Used in the Twitter card that appears when you share a post on Twitter. Also in the page footer.
```
"mastodon": {
	"host": "masto.example.com",
	"name": "Indie"
},
"pixelfed": {
	"host": "pix.example.com",
	"name": "IndiesPix"
},
```
Mastodon or PixelFed accounts that the author wants to include in the footer or in the preview card byline [when shared on newer versions of Mastodon](https://blog.joinmastodon.org/2024/07/highlighting-journalism-on-mastodon/). The links will be generated using the usual patterns. The byline only supports one account, so if you set multiple, it will choose in this order: fediverse, mastodon, pixelfed.

```
"fediverse": {
	"url": "https://somefed.example.com/u/@Indies/",
	"name": "Indies@Example.com"
}
```
Any other Fediverse account. Needs the full URL to the author's profile, not just their name.

**The rest of these are only used in the footer.**

```
"facebook": "Example",
"linkedin": "Example",
```
The author's usernames at each site.

```
"email": "indie@example.com",
```
A contact email the author is willing to list.

```
"blog": "https://indie.example.com/blog/",
```
A separate blog URL, if they want to link to a blog other than their home page.

You can, of course, add and remove items to the footer as you wish by modifying `includes/footer.njk`


[Source on Codeberg](https://codeberg.org/kvibber/indies-eleven)   
(c) 2022-2024 Kelson Vibber [kvibber.com](https://kvibber.com) [software@kvibber.com](mailto:software@kvibber.com)
