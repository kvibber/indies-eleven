---
title: "Page Information in Indie's Eleven"
date: "2022-06-16T20:10:00-0700"
minorUpdate: "2023-02-26T21:00:00-0700"
layout: article.njk
schemaType: TechArticle
authorId: KelsonV
---

## Basic Page Info

Titles, author info, and so on all go in the "front matter" at the beginning of the Markdown file. It looks something like this:

```
---
title: "The Title of the Page"
description: "This is an example page with just some random text"
date: "2022-06-17T11:00:00-0700"
layout: article.njk
author:
  name: "Full Name"
  url: https://example.com/fullname
authorId: AuthorsNicknameFromAuthors.json
---
```

You don't need to use both author and authorId. And if you have a site-wide author in siteInfo.json, you can leave both of them out.


## Dates

You can leave out the date field, and it will use the file's modification date. This is convenient, but may not always be what you want.

Updates can be labeled in two different ways. Eleventy sorts all the posts in date order for next/previous, viewing as a stream, showing the latest posts, and building the feed of recent posts.

**To change the page's position in the site's sort order**  

```
date: "2023-02-26T21:00:00-0700"
originalDate: "2022-06-16T20:00:00-0700" 
```

**To keep the page where it was in the sort order.**

```
date: "2022-06-16T20:00:00-0700"
minorUpdate: "2023-02-26T21:00:00-0700"
```

In both cases the byline (and metadata) will show it was posted June 16, 2022 and updated February 26, 2023. 

## More Detail

categories, tags, etc

## Related Pages

related, alternates, canonical, etc.

## Post Types

### Article

`layout: article.njk`

### Photo

`layout: photo.njk`
Set the title as usual, and write the caption as long as you want in the post body.

```
image: path/to/original-image
alt: "Description for low-vision readers."
```

Hint: For the alt description, imagine a screen reader is going to read the whole thing aloud, and focus on the parts you want to get across.

### Review
`layout: review.njk`

Basically the same as an Article, plus these optional fields:

```
stars: Rating from 1 to 5 (5 is best). Will appear as stars in the post and feed.
aboutUrl: Link to what you're reviewing.
aboutSchemaType: Schema.org type of what you're reviewing in a Review post. Only used in metadata (see below)
```

## Metadata / Link Previews

You know those previews you see when sharing a link on Facebook, Twitter, etc.? They're not usually built directly from the page, but from metadata fields in the document header. Indie's Eleven will build data for [OpenGraph](https://ogp.me/) (Facebook, LinkedIn, etc.), [Twitter Cards](https://developer.twitter.com/en/docs/twitter-for-websites/cards/overview/markup), Schema.org (Google) and [Microformats2](http://microformats.org/wiki/microformats2) ([IndieWeb](https://indieweb.org/) compatible sites), as well as standard HTML information.

I11 will fill in as much as it can from the fields listed above, plus additional information in the layouts. But if you want to set something more specific, you can set these per-page

ogType (OG): In case "website" or "article" aren't quite right.
schemaType (S): Maybe you want to set "TechArticle" or something else more specific.

### Test Your Previews

[OpenGraph Check](https://www.opengraphcheck.com/)
[LinkedIn Post Inspector](https://www.linkedin.com/post-inspector/)
[Twitter Card Validator](https://cards-dev.twitter.com/validator)
[Google Rich Results Test](https://search.google.com/test/rich-results)
[Schema.org Validator](https://validator.schema.org/)
[IndieWebify.Me](https://indiewebify.me/)


[Source on Codeberg](https://codeberg.org/kvibber/indies-eleven)   
(c) 2022-2023 Kelson Vibber [kvibber.com](https://kvibber.com) [software@kvibber.com](mailto:software@kvibber.com)
