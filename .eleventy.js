const pluginRss = require("@11ty/eleventy-plugin-rss");
const { DateTime } = require("luxon");
const markdownIt = require("markdown-it");
const markdownItAttrs = require("markdown-it-attrs");

const moment = require("moment");
const path = require("path");
const striptags = require("striptags");
const Image = require("@11ty/eleventy-img");

// GETTING_STARTED: Set the folder you want to store images, the formats
// to generate, and the sizes to generate.
async function imageShortcode(src, alt, sizes, link, clazz, mainWidth = 600, eleventyConfig) {
  const urlFilter = eleventyConfig.getFilter("url");
  fallbackType="jpeg";
  regex = /\.(png|gif)/;
  if (regex.test(src)) { fallbackType = "png" }
  let metadata = await Image(src, {
    widths: [300, mainWidth, 900, 1200],
    formats: ["avif", "webp", fallbackType],
    urlPath: urlFilter("/img/"),
    filenameFormat: function (id, src, width, format, options) {
  	const extension = path.extname(src);
  	const name = path.basename(src, extension);
  	return `${name}-${width}w.${format}`;
    }
  });

  let imageAttributes = {
    alt,
    sizes,
    decoding: "async",
    class: clazz
  };

  // You bet we throw an error on missing alt in `imageAttributes` (alt="" works okay)
  var html = Image.generateHTML(metadata, imageAttributes);
  
  if (link == "link") {
     return '<a href="' + urlFilter('/'+ src) + '">' + html + '</a>';
  } else if (link == "nolink" || link == "") {
     return html;
  } else {
     return '<a href="' + link + '">' + html + '</a>';
  }
}

async function siteIconShortcode(src, size, eleventyConfig) {
  const extension = path.extname(src);
  const name = path.basename(src, extension);
  const urlPath = "/img/";
  const urlFilter = eleventyConfig.getFilter("url");
  let metadata = await Image(src, {
    widths: [32, 128, 180, 192, 270, 300, 1200],
    formats: [ null ],
    urlPath: urlPath,
    filenameFormat: function (id, src, width, format, options) {
  	return `${name}-${width}w.${format}`;
    }
  });
  basePath = `${urlPath}${name}-${size}w${extension}`
  return urlFilter(basePath);
}



// Sort in reverse-alphabetical order so when 11ty reverses it again, it'll be forward.
function titleSort(a,b) {
    titleA = a.data.title.replace(/^(A|The) /, ''); 
    titleB = b.data.title.replace(/^(A|The) /, ''); 
	if (titleA > titleB) return 1;
	else if (titleA < titleB) return -1;
	else return 0;
}

// Sort in chapter order.
function chapterSort(a,b) {
	if (a.data.chapter > b.data.chapter) return 1;
	else if (a.data.chapter < b.data.chapter) return -1;
	else return 0;
}

// Show star rating using emoji.
function starsShortcode(count, total=5) {
     count = Math.floor(count);
     total = Math.floor(total);
     alt = count + ' stars out of ' + total;
     return '<abbr class="p-rating" value="' + count + '" title="'+ alt +'" aria-label="'+ alt +'.">'
      + starsRawShortcode(count,total)
      + '</abbr>';
}

function starsRawShortcode(count, total=5) {
     count = Math.floor(count);
     total = Math.floor(total);
      return '★'.repeat(count)
      + '☆'.repeat(total-count);
}

// Build an author h-card for the page's author.
function authorCardShortcode(author) {
     // If we have a URL, build a link. Otherwise, a span.
     if (author.url) {
     	return '<a class="p-author h-card" rel="author" href="' + author.url + '">' + author.name + '</a>';
     } else {
     	return '<span class="p-author h-card">' + author.name + '</span>';
     }
}

module.exports = function(eleventyConfig) {
	// GETTING_STARTED
	// TODO build from categories.json and siteInfo.json instead
	var catlist = ['docs', 'examples','notes'];
	var localTimeZone='America/Los_Angeles';


	
	var md = require("markdown-it");
	var mdAttrs = require("markdown-it-attrs");

	eleventyConfig.addPlugin(pluginRss);

	eleventyConfig.addPassthroughCopy("images");
	eleventyConfig.addPassthroughCopy("img");
	eleventyConfig.addPassthroughCopy("*.css");

	// Don't include drafts. TODO make this conditional on build mode.
	eleventyConfig.ignores.add("_drafts");

	// Full list of post sorted by newest first.
	eleventyConfig.addCollection("posts", (collection) =>
	    collection.getFilteredByGlob(["*.md","*/*.md"])
	);

	// Only posts within the last 90 days.
	eleventyConfig.addCollection("latest", (collection) =>
	    collection.getFilteredByGlob(["*.md","*/*.md"]).filter(function(post){
		var oldest = moment().subtract(90, 'days').toDate();
	    	if ( post.date >= oldest )
		    	return true
	    	else
	    		return false
	    })
	);

	// Build category collections and sort each alphabetically by title.
	for (let cat of catlist) {
		eleventyConfig.addCollection(cat, (collection) =>
			collection.getFilteredByGlob(cat + "/*.md").sort(titleSort)
		);
	}

	// Add shortcodes for functions listed above.
	eleventyConfig.addAsyncShortcode("image", function(src, alt, sizes, link, clazz, mainWidth){
		return imageShortcode(src, alt, sizes, link, clazz, mainWidth, eleventyConfig)
	});
	eleventyConfig.addJavaScriptFunction("image", function(src, alt, sizes, link, clazz, mainWidth){
		return imageShortcode(src, alt, sizes, link, clazz, mainWidth, eleventyConfig)
	});
	eleventyConfig.addAsyncShortcode("siteIcon", function (src, size) {
		return siteIconShortcode(src, size, eleventyConfig);
	});
	eleventyConfig.addShortcode("stars", starsShortcode);
	eleventyConfig.addShortcode("starsRaw", starsRawShortcode);
	eleventyConfig.addJavaScriptFunction("stars", starsShortcode);
	eleventyConfig.addShortcode("authorCard", authorCardShortcode);

	// Render <aside>...</aside> and parse the markdown inside it.
	eleventyConfig.addPairedShortcode("aside", function(content, clazz) {
		classMarkup = (clazz) ? ' class="' + clazz + '"' : '';
		return '<aside' + classMarkup + '>' + markdownLibrary.render(content) + '</aside>';
	});

	// Make tags suitable for use as hashtags
	eleventyConfig.addFilter("camelCase", toSlug => {
		return eleventyConfig.getFilter('slugify')(toSlug, { customReplacements: [ ["'",''] ] }).replace(/(-\w)/g, k => k[1].toUpperCase());
	});

	// get the first n words of a string
	eleventyConfig.addFilter('headWords', (string, n) => {
		return striptags(string).split(/\s+/).slice(0,n).join(' ');
	});

	// Below adapted from eleventy-base-blog

	  /* Markdown Overrides */
	  let markdownLibrary = markdownIt({
	    html: true,
	    breaks: true,
	    linkify: true,
	    typographer: true
	  }).use(markdownItAttrs, {
	    permalink: true,
	    permalinkClass: "direct-link",
	    permalinkSymbol: "#"
	  });
	  eleventyConfig.setLibrary("md", markdownLibrary);
	  
	  eleventyConfig.addFilter("parseSortableDate", dateString => {
	    return new Date(dateString);
	  });
	  
	  eleventyConfig.addFilter("monthYear", dateObj => {
  	    localDatestamp = DateTime.fromJSDate(dateObj, {zone: localTimeZone});
  	    options = { year: 'numeric', month: 'long' }
	    return localDatestamp.toLocaleString(options);
	  });
  	  eleventyConfig.addFilter("simpleDate", dateObj => {
	    return DateTime.fromJSDate(dateObj, {zone: localTimeZone}).toLocaleString(DateTime.DATE_SHORT);
	  });
	  eleventyConfig.addFilter("readableDate", dateObj => {
	    return DateTime.fromJSDate(dateObj, {zone: localTimeZone}).toLocaleString(DateTime.DATE_FULL);
	  });
	  eleventyConfig.addFilter("readableTime", dateObj => {
	    return DateTime.fromJSDate(dateObj, {zone: localTimeZone}).toLocaleString(DateTime.TIME_SIMPLE);
	  });
	  eleventyConfig.addFilter("isoDate", dateObj => {
	    return dateObj.toISOString();
	  });
	  eleventyConfig.addFilter("urlEncodePart", sourceString => {
	    return encodeURIComponent(sourceString);
	  });
	  eleventyConfig.addFilter("markdownRender", segment => {
	    return markdownLibrary.render(segment);
	  });
	  
	  // Get the first `n` elements of a collection.
	  eleventyConfig.addFilter("head", (array, n) => {
	    if( n < 0 ) {
	      return array.slice(n);
	    }

	    return array.slice(0, n);
	  });

	// Build a collection of all tag pages.
	eleventyConfig.addCollection("tagList", function(collection) {
	    let tagSet = new Set();
	    collection.getAll().forEach(function(item) {
	      if( "tags" in item.data ) {
		let tags = item.data.tags;

		tags = tags.filter(function(item) {
		  switch(item) {
		    // this list should match the `filter` list in tags.njk
		    case "all":
		    case "nav":
		    case "post":
		    case "posts":
		      return false;
		  }

		  return true;
		});

		for (const tag of tags) {
		  tagSet.add(tag);
		}
	      }
	    });

	    // returning an array in addCollection works in Eleventy 0.5.3
	    return [...tagSet].sort();
	  });

	  // Render markdown inside a variable.
	  eleventyConfig.addFilter("mdInline", input => {
	    return markdownLibrary.renderInline(input);
	  });

	  return {
	    dir: {
	      output: "_site",
	    }
	  }
};
